stages:
  - build
  - test
  - deploy

# Build Python packages
build:
  image: quay.io/pypa/manylinux1_x86_64
  stage: build
  script:
    - /opt/python/cp36-cp36m/bin/python setup.py sdist -d . bdist_wheel -d .
  artifacts:
    paths:
      - '*.whl'
      - '*.tar.gz'
    expire_in: 5 minutes

test:
  image: python:3.6
  stage: test
  coverage: '/^TOTAL\s+.*\s+(\d+\.?\d*)%/'
  variables:
    GIT_STRATEGY: none
  script:
    - pip install astropy pytest-cov
    - tar --strip-components 1 -xf *.tar.gz
    - python setup.py test --addopts='-vv -s --cov --cov-report=html --cov-report=term'
  dependencies:
    - build
  artifacts:
    paths:
      - htmlcov
      - '*.tar.gz'
    expire_in: 5 minutes

# Run code lint
lint:
  image: python:3.6
  stage: test
  variables:
    GIT_STRATEGY: none
  script:
    - tar --strip-components 1 -xf *.tar.*
    - pip install flake8
    - flake8 --show-source .
  dependencies:
    - build

# Publish coverage
pages:
  stage: deploy
  script:
    - mv htmlcov public/
  artifacts:
    paths:
      - public
    expire_in: 30 days
  dependencies:
    - test
  only:
    - master

# Upload package to PyPI
pypi:
  stage: deploy
  image: python:slim
  variables:
    GIT_STRATEGY: none
  script:
    - pip install twine
    - twine upload *.tar.* *.whl
  dependencies:
    - build
  only:
    - tags@emfollow/ligo-followup-advocate
