# Changelog

## 0.0.11 (unreleased)

-   No changes yet.

## 0.0.10 (2018-06-28)

-   Changed how VOEvent text is pulled down; now using `client.files()` method
    versus relying on `client.voevents()` json response.

## 0.0.9 (2018-06-28)

-   Circulars are generated strictly for superevents.

## 0.0.8 (2018-06-27)

-   GraceDb links in circulars now reflect the URL of the GraceDb server from
    which the event originated.

## 0.0.7 (2018-05-24)

-   Drop support for Python 2.

-   Do not print the circular if calling `compose()` from a Python script.

-   Remove duplicate skymaps and keep only the lowest latency ones.

-   Do not include skymaps produced outside the LVC.

-   Optional GraceDb client keyword argument when calling compose().

## 0.0.6 (2018-05-08)

-   Make it easier to call functions from Python scripts, like this:

        >>> from ligo import followup_advocate
        >>> text = followup_advocate.compose('G299232')

-   Add `--service` command line option to set the GraceDB service URL.

## 0.0.5 (2018-05-03)

-   First version released on PyPI.
